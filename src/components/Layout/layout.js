import Head from "next/head";
import Image from "next/image";

import styles from '../../styles/Home.module.css'


import utilStyles from "../../styles/utils.module.css";
import Link from "next/link";

import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import SportsFootballIcon from '@mui/icons-material/SportsFootball';

const name = "Welcom";
export const siteTitle = "Perfect Sport Network - Dashboard";

export default function Layout({ children, home }) {
  return (
    <div className={styles.container}>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="description"
          content="Learn how to build a personal website using Next.js"
        />
        <meta
          property="og:image"
          content={`https://og-image.vercel.app/${encodeURI(
            siteTitle
          )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
        />
        <meta name="og:title" content={siteTitle} />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <header className={styles.header}>
        {home ? (
          <>
            <Box
              sx={{
                display: "flex",
                flexWrap: "wrap",
                "& > :not(style)": {
                  m: 1,
                  width: "100%",
                  height: 128,
                },
              }}
            >
              
                <Grid container wrap="nowrap" spacing={2}>
                  <Grid item>
                    <Image
                      priority
                      src="/images/profile.jpg"
                      className={utilStyles.borderCircle}
                      height={90}
                      width={90}
                      alt={name}
                    />
                  </Grid>
                  <Grid item xs>
                    <h3 className={utilStyles.heading}>{name}<br/>Dominic Quintana</h3>
                  </Grid>
                </Grid>
           
            </Box>
          </>
        ) : (
          <>
            <Link href="/">
              <a>
                <Image
                  priority
                  src="/images/profile.jpg"
                  className={utilStyles.borderCircle}
                  height={108}
                  width={108}
                  alt={name}
                />
              </a>
            </Link>
            <h2 className={utilStyles.headingLg}>
              <Link href="/">
                <a className={utilStyles.colorInherit}>{name}</a>
              </Link>
            </h2>
          </>
        )}
      </header>
      <main>{children}</main>
      {home && (
        <div className={styles.backToHome}>
          <Link href="/">
            <a>← Back to home</a>
          </Link>
        </div>
      )}
      <footer className={styles.footer}>
      <a
          href="#"
          rel="noopener noreferrer"
        >
          <span className={styles.footer2}>
            <SportsFootballIcon fontSize="large" />
            <p>Copyright © - All rights reserved - Produced by</p>
            <p>Perfect Sport Network 2022</p>             
          </span>
        </a>
      </footer>
    </div>
  );
}
