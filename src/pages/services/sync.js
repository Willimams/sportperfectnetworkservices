import Head from 'next/head'
import Image from 'next/image'

import Link from 'next/link'
import Layout from '../../components/Layout/layout'
import utilStyles from '../../styles/utils.module.css'

//import * as React from 'react';
import React, { useReducer, useState } from 'react'

import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import Switch from '@mui/material/Switch';
import WifiIcon from '@mui/icons-material/Wifi';
import BluetoothIcon from '@mui/icons-material/Bluetooth';
import Divider from '@mui/material/Divider';
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';
import { green } from '@mui/material/colors';
import Button from '@mui/material/Button';
import Fab from '@mui/material/Fab';
import CheckIcon from '@mui/icons-material/Check';
import SaveIcon from '@mui/icons-material/Save';
import SendIcon from '@mui/icons-material/Send';


import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

import styles from '../../styles/sync.module.css';
import { Typography } from '@mui/material'
//import styles from '../../styles/Home.module.css'

import Grid from "@mui/material/Grid";

import AdapterDateFns from '@mui/lab/AdapterDateFns';
import TextField from '@mui/material/TextField';

import LocalizationProvider from '@mui/lab/LocalizationProvider';
//import TimePicker from '@mui/lab/TimePicker';
//import DateTimePicker from '@mui/lab/DateTimePicker';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';

import Stack from '@mui/material/Stack';

import AxiosInstance from '../../components/config/axiosinstance'

const Sync = ({ allPostsData }) => {

  const [currentDateLeft, setCurrentDateLeft] = useState(new Date());
  const [currentDateRight, setCurrentDateRight] = useState(new Date());

  const [open, setOpen] = React.useState(false);

  const [checked, setChecked] = React.useState({
    valueCurrentSeason: ['wifi'],
    valueTeamClient: ['wifi'],
    valueTeamSchedule: ['wifi'],
    valueBettingSplitsByGameId: ['wifi'],
    valueTeamTrend: ['wifi'],
    valueLeagueHierarchy: ['wifi'],
    valueGameOddsLineMovement: ['wifi'],
    valueGamesByDate: ['wifi'],
    valueInjuredPlayers: ['wifi'],
    valueTeamGameStatsBySeason: ['wifi'],
    valueLiveGameOddsLineMovement: ['wifi'],
    valueBettingMetadata: ['wifi'],
    valueBettingMarketsByGameID: ['wifi'],
    valueBettingMarkets: ['wifi'],
    valueStadiums: ['wifi'],

  });





  const [loading, setLoading] = React.useState({
    valueCurrentSeason: false,
    valueTeamClient: false,
    valueTeamSchedule: false,
    valueBettingSplitsByGameId: false,
    valueTeamTrend: false,
    valueLeagueHierarchy: false,
    valueGameOddsLineMovement: false,
    valueGamesByDate: false,
    valueInjuredPlayers: false,
    valueTeamGameStatsBySeason: false,
    valueLiveGameOddsLineMovement: false,
    valueBettingMetadata: false,
    valueBettingMarketsByGameID: false,
    valueBettingMarkets: false,
    valueStadiums: false,
  });

  const [success, setSuccess] = React.useState({
    valueCurrentSeason: false,
    valueTeamClient: false,
    valueTeamSchedule: false,
    valueBettingSplitsByGameId: false,
    valueTeamTrend: false,
    valueLeagueHierarchy: false,
    valueGameOddsLineMovement: false,
    valueGamesByDate: false,
    valueInjuredPlayers: false,
    valueTeamGameStatsBySeason: false,
    valueLiveGameOddsLineMovement: false,
    valueBettingMetadata: false,
    valueBettingMarketsByGameID: false,
    valueBettingMarkets: false,
    valueStadiums: false,
  });


  /*
    const handleToggle = (value) => () => {
      const currentIndex = checked.indexOf(value);
      console.log("currrenteIndex: " + currentIndex);
      const newChecked = [...checked];
      console.log(newChecked);
  
      if (currentIndex === -1) {
        newChecked.push(value);
        console.log(newChecked);
      } else {
        newChecked.splice(currentIndex, 1);
      }
      setChecked(newChecked);
      console.log(newChecked);
    };
  */
  const handleToggle = (value, name_arr) => () => {
    console.log("entre_Aqui...........")
    const currentIndex = checked[name_arr].indexOf(value);
    console.log(currentIndex);

    const newChecked = [...checked[name_arr]];
    console.log(newChecked);

    if (currentIndex === -1) {
      newChecked.push(value);
      console.log(newChecked);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    //setCheckeds(newChecked);
    setChecked({ ...checked, [name_arr]: newChecked });
    console.log(newChecked);
  };



  const buttonSx = (name) => {
    //console.log(name);
    return {
      ...(success[name] && {
        bgcolor: green[500],
        '&:hover': {
          bgcolor: green[700],
        },
      })
    };
  }



  const handleClickOpen = () => {
    console.log("card open");
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };



  const Client = async (name_value, url, e) => {
    e.preventDefault();
    console.log(name_value);
    //console.log(url);
    //console.log("valores... : ", loadings[name_value]) 

    if (!loading[name_value]) {

      setSuccess({ ...success, [name_value]: false });
      setLoading({ ...loading, [name_value]: true });
      // setSuccess(false);
      // setLoading(true);

      //const photo = AxiosInstance.get("/CurrentSeasonClient").then((response) => 
      const photo = await AxiosInstance.get(url).then((response) =>
        //setPost(response.data);
        response.data
        //console.log(response.data)
      );
      /*
      const photo = await fetch(
        url

      ).then((response) => response.json());
      */
      if (photo.Season === 2022 && photo.StartYear === 2021) {
        handleClickOpen();
      } else {
      }

      setSuccess({ ...success, [name_value]: true });
      setLoading({ ...loading, [name_value]: false });



    }

  }

  const ClientFlow = async (name_value, url, e) => {
    e.preventDefault()
    console.log(name_value);

    if (!loading[name_value]) {

      setSuccess({ ...success, [name_value]: false });
      setLoading({ ...loading, [name_value]: true });

      const dataApi  = await AxiosInstance.get(url).then((response) =>
        //setPost(response.data);
        response.data
        // console.log(response.data)
      );        
      /* const dataApi = await fetch(
        "https://sportsdataclient.herokuapp.com/InjuredPlayersClient"
  
        ).then((response) => response.json());*/
      console.log("data Api ", dataApi)
      console.log("Valor de continue flow ", dataApi.continueFlow)
      if (dataApi.continueFlow === "TRUE") {
        console.log("Entre aqui ");

        handleClickOpen();
      } else {
        console.log("Error to download.");
      }

      setSuccess({ ...success, [name_value]: true });
      setLoading({ ...loading, [name_value]: false });

    }

  }
  // CurrentSeasonClient
  // `https://sportsdataclient.herokuapp.com/CurrentSeasonClient`

  // TeamClient /
  // `https://sportsdataclient.herokuapp.com/TeamClient`


  // TeamScheduleClient
  // `https://sportsdataclient.herokuapp.com/TeamScheduleClient`

  // BettingSplitsByGameIdClient
  // `https://sportsdataclient.herokuapp.com/BettingSplitsByGameIdClient/30478`

  // TeamTrendClient
  // `https://sportsdataclient.herokuapp.com/TeamTrendClient`

  // LeagueHierarchyClient
  // `https://sportsdataclient.herokuapp.com/LeagueHierarchyClient`

  // GameOddsLineMovementClient/
  //    `https://sportsdataclient.herokuapp.com/GameOddsLineMovementClient/30478`

  // GamesByDateClient
  //`https://sportsdataclient.herokuapp.com/GamesByDateClient/2021-03-12`

  // InjuredPlayersClient
  //     `https://sportsdataclient.herokuapp.com/InjuredPlayersClient`

  // TeamGameStatsBySeasonClient/  
  //  `https://sportsdataclient.herokuapp.com/TeamGameStatsBySeasonClient/2022/268`

  // LiveGameOddsLineMovementServer/30458/  Flow
  //   `https://sportsdataclient.herokuapp.com/LiveGameOddsLineMovementClient/30460`

  //BettingMetadataClient Flow
  //    `https://sportsdataclient.herokuapp.com/BettingMetadataClient`

  // //BettingMarketsByGameIDClient/ Flow
  //    `https://sportsdataclient.herokuapp.com/BettingMarketsByGameIDClient/30479`

  // BettingMarketsClient Flow
  //     `https://sportsdataclient.herokuapp.com/BettingMarketsClient/4697`

  // StadiumsClient Flow
  //   `https://sportsdataclient.herokuapp.com/StadiumsClient`

  /*Date management */

  /*Manejo de fechas */
  const handleChangeLeft = (fecha) => {
       /*Actulizadon la fechas */
    setCurrentDateLeft(fecha);

  };

  const handleChangeRight = (fecha) => {
        /*Actulizadon la fechas */
    setCurrentDateRight(fecha);

  };

  const getDay = (tempDateDay) => {
    let res = "0";
    if (tempDateDay > 0 && tempDateDay < 10) {
      res = res + tempDateDay;
      return res;
    } else {
      return tempDateDay;
    }
  }

  const getMonth = (tempDateMonth) => {
    let res = "0";
    if (tempDateMonth > 0 && tempDateMonth < 10) {
      let temp = tempDateMonth + 1;
      res = res + temp;
      return res;
    } else {
      return tempDateMonth + 1;
    }
  }

  return (
    <div className={styles.container}>


      <Layout home>
        <Head>
          <title>Sport Perfect Network - Services</title>
        </Head>


        <section className={utilStyles.headingMd}>
          <Typography sx={{ fontSize: '30px', fontWeight: '500', marginBottom: '14px' }} component='div' variant='h1'>Available Services</Typography>


          <List
            className={styles.list}
            subheader={<ListSubheader>Settings</ListSubheader>}
          >

            {//Service::::::::  CurrentSeason  "https://api.sportsdata.io/v3/cbb/scores/json/CurrentSeasonClient"
            }
            <ListItem className={styles.listItem}>
              <Typography className={styles.listItemhead} component="div" variant='div'>
                <ListItemIcon><WifiIcon /></ListItemIcon>
                <ListItemText className={styles.title} id="switch-list-label-wifi" primary="Current Season"


                  secondary={<Typography component="div" variant='h5' className={styles.subtitle}
                  >https://api.sportsdata.io/v3/cbb/scores/json/CurrentSeasonClient</Typography>} />
              </Typography>
              <Typography sx={{ display: 'flex' }} component="div" variant='div' >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Box sx={{ m: 1, position: 'relative' }}>
                    <Button variant="contained" sx={buttonSx("valueCurrentSeason")} disabled={loading.valueCurrentSeason} endIcon={<SendIcon />}
                      onClick={(e) => Client("valueCurrentSeason", "/CurrentSeasonClient",
                        e)}>
                      {success.valueCurrentSeason ? <CheckIcon /> : <SaveIcon />}
                      Download Data
                    </Button>
                    {loading.valueCurrentSeason && (
                      <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                      />
                    )}
                  </Box>
                </Box>
                <Switch
                  edge="end" onChange={handleToggle('bluetooth', "valueCurrentSeason")} checked={checked.valueCurrentSeason.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
                />
              </Typography>
            </ListItem>
            <Divider />

            {//Service::::::::  TeamClient 
            }
            <ListItem className={styles.listItem}>

              <Typography className={styles.listItemhead} component="div" variant='div'>
                <ListItemIcon><WifiIcon /></ListItemIcon>
                <ListItemText className={styles.title} id="switch-list-label-wifi" primary="TeamClient"
                  secondary={<Typography component="div" variant='h5' className={styles.subtitle}
                  >https://api.sportsdata.io/v3/cbb/scores/json/TeamClient</Typography>}
                />
              </Typography>

              <Typography sx={{ display: 'flex' }} component="div" variant='div' >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Box sx={{ m: 1, position: 'relative' }}>
                    <Button variant="contained" sx={buttonSx("valueTeamClient")} disabled={loading.valueTeamClient} endIcon={<SendIcon />}
                      onClick={(e) => ClientFlow("valueTeamClient", "/TeamClient",
                        e)}>
                      {success.valueTeamClient ? <CheckIcon /> : <SaveIcon />}
                      Download Data
                    </Button>
                    {loading.valueTeamClient && (
                      <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                      />
                    )}
                  </Box>
                </Box>
                <Switch
                  edge="end" onChange={handleToggle('bluetooth', "valueTeamClient")} checked={checked.valueTeamClient.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
                />
              </Typography>
            </ListItem>
            <Divider />


            {//Service::::::::  teamScheduleClient  
            }
            <ListItem className={styles.listItem}>
              <Typography className={styles.listItemhead} component="div" variant='div'>
                <ListItemIcon><WifiIcon /></ListItemIcon>
                <ListItemText className={styles.title} id="switch-list-label-wifi" primary="TeamScheduleClient"

                  secondary={<Typography component="div" variant='h5' className={styles.subtitle}
                  >https://api.sportsdata.io/v3/cbb/scores/json/TeamScheduleClient</Typography>}
                />
              </Typography>
              <Typography sx={{ display: 'flex' }} component="div" variant='div' >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Box sx={{ m: 1, position: 'relative' }}>
                    <Button variant="contained" sx={buttonSx("valueTeamSchedule")} disabled={loading.valueTeamSchedule} endIcon={<SendIcon />}
                      onClick={(e) => Client("valueTeamSchedule", "/CurrentSeasonClient",
                        e)}>
                      {success.valueTeamSchedule ? <CheckIcon /> : <SaveIcon />}
                      Download Data
                    </Button>
                    {loading.valueTeamSchedule && (
                      <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                      />
                    )}
                  </Box>
                </Box>
                <Switch
                  edge="end" onChange={handleToggle('bluetooth', "valueTeamSchedule")} checked={checked.valueTeamSchedule.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
                />
              </Typography>
            </ListItem>
            <Divider />
            {//Service::::::::  BettingSplitsByGameIdClient  
            }
            <ListItem className={styles.listItem}>
              <Typography className={styles.listItemhead} component="div" variant='div'>
                <ListItemIcon><WifiIcon /></ListItemIcon>
                <ListItemText className={styles.title} id="switch-list-label-wifi" primary="BettingSplitsByGameIdClient"

                  secondary={<Typography component="div" variant='h5' className={styles.subtitle}
                  >https://api.sportsdata.io/v3/cbb/scores/json/BettingSplitsByGameIdClient</Typography>}
                />
              </Typography>
              <Typography sx={{ display: 'flex' }} component="div" variant='div' >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Box sx={{ m: 1, position: 'relative' }}>
                    <Button variant="contained" sx={buttonSx("valueBettingSplitsByGameId")} disabled={loading.valueBettingSplitsByGameId} endIcon={<SendIcon />}
                      onClick={(e) => Client("valueBettingSplitsByGameId", "/BettingSplitsByGameIdClient/30478",
                        e)}>
                      {success.valueBettingSplitsByGameId ? <CheckIcon /> : <SaveIcon />}
                      Download Data
                    </Button>
                    {loading.valueBettingSplitsByGameId && (
                      <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                      />
                    )}
                  </Box>
                </Box>
                <Switch
                  edge="end" onChange={handleToggle('bluetooth', "valueBettingSplitsByGameId")} checked={checked.valueBettingSplitsByGameId.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
                />
              </Typography>
            </ListItem>
            <Divider />

            {//Service::::::::  TeamTrendClient  
            }
            <ListItem className={styles.listItem}>
              <Typography className={styles.listItemhead} component="div" variant='div'>
                <ListItemIcon><WifiIcon /></ListItemIcon>
                <ListItemText id="switch-list-label-wifi" primary="TeamTrendClient"

                  secondary={<Typography component="div" variant='h5' className={styles.subtitle}
                  >https://api.sportsdata.io/v3/cbb/scores/json/TeamTrendClient</Typography>}
                />
              </Typography>
              <Typography sx={{ display: 'flex' }} component="div" variant='div' >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Box sx={{ m: 1, position: 'relative' }}>
                    <Button variant="contained" sx={buttonSx("valueTeamTrend")} disabled={loading.valueTeamTrend} endIcon={<SendIcon />}
                      onClick={(e) => Client("valueTeamTrend", "https://sportsdataclient.herokuapp.com/BettingSplitsByGameIdClient/30478",
                        e)}>
                      {success.valueTeamTrend ? <CheckIcon /> : <SaveIcon />}
                      Download Data
                    </Button>
                    {loading.valueTeamTrend && (
                      <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                      />
                    )}
                  </Box>
                </Box>
                <Switch
                  edge="end" onChange={handleToggle('bluetooth', "valueTeamTrend")} checked={checked.valueTeamTrend.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
                />
              </Typography>
            </ListItem>
            <Divider />

            {//Service::::::::  LeagueHierarchyClient  
            }
            <ListItem className={styles.listItem}>
              <Typography className={styles.listItemhead} component="div" variant='div'>
                <ListItemIcon><WifiIcon /></ListItemIcon>
                <ListItemText id="switch-list-label-wifi" primary="LeagueHierarchyClient" secondary=" https://api.sportsdata.io/v3/cbb/scores/json/LeagueHierarchyClient" />
              </Typography>
              <Typography sx={{ display: 'flex' }} component="div" variant='div' >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Box sx={{ m: 1, position: 'relative' }}>
                    <Button variant="contained" sx={buttonSx("valueLeagueHierarchy")} disabled={loading.valueLeagueHierarchy} endIcon={<SendIcon />}
                      onClick={(e) => ClientFlow("valueLeagueHierarchy", "/LeagueHierarchyClient",
                        e)}>
                      {success.valueLeagueHierarchy ? <CheckIcon /> : <SaveIcon />}
                      Download Data
                    </Button>
                    {loading.valueLeagueHierarchy && (
                      <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                      />
                    )}
                  </Box>
                </Box>
                <Switch
                  edge="end" onChange={handleToggle('bluetooth', "valueLeagueHierarchy")} checked={checked.valueLeagueHierarchy.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
                />
              </Typography>
            </ListItem>
            <Divider />

            {//Service::::::::  GameOddsLineMovementClient  
            }
            <ListItem className={styles.listItem}>
              <Typography className={styles.listItemhead} component="div" variant='div'>
                <ListItemIcon><WifiIcon /></ListItemIcon>
                <ListItemText id="switch-list-label-wifi" primary="GameOddsLineMovementClient" secondary="https://api.sportsdata.io/v3/cbb/scores/json/GameOddsLineMovementClient" />
              </Typography>
              <Typography sx={{ display: 'flex' }} component="div" variant='div' >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>

                  <Box sx={{ m: 1, position: 'relative' }}>
                    <Button variant="contained" sx={buttonSx("valueGameOddsLineMovement")} disabled={loading.valueGameOddsLineMovement} endIcon={<SendIcon />}
                      onClick={(e) => ClientFlow("valueGameOddsLineMovement", "https://sportsdataclient.herokuapp.com/GameOddsLineMovementClient/30478",
                        e)}>
                      {success.valueGameOddsLineMovement ? <CheckIcon /> : <SaveIcon />}
                      Download Data
                    </Button>
                    {loading.valueGameOddsLineMovement && (
                      <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                      />
                    )}
                  </Box>
                </Box>
                <Switch
                  edge="end" onChange={handleToggle('bluetooth', "valueGameOddsLineMovement")} checked={checked.valueGameOddsLineMovement.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
                />
              </Typography>
            </ListItem>
            <Divider />

            {//Service::::::::  GamesByDateClient  
            }
            <ListItem className={styles.listItem}>
              <Typography className={styles.listItemhead} component="div" variant='div'>
                <ListItemIcon><WifiIcon /></ListItemIcon>
                <ListItemText id="switch-list-label-wifi" primary="GamesByDateClient" secondary="https://api.sportsdata.io/v3/cbb/scores/json/GamesByDateClient" />
              </Typography>

              <LocalizationProvider dateAdapter={AdapterDateFns} >
                <Stack spacing={3}>
                  <DesktopDatePicker

                    inputFormat="MM/dd/yyyy"
                    value={currentDateLeft}
                    onChange={handleChangeLeft}
                    renderInput={(params) => <TextField {...params} />}
                  />
                </Stack>
              </LocalizationProvider>
              <LocalizationProvider dateAdapter={AdapterDateFns} >
                <Stack spacing={3}>
                  <DesktopDatePicker

                    inputFormat="MM/dd/yyyy"
                    value={currentDateRight}
                    onChange={handleChangeRight} 
                    renderInput={(params) => <TextField {...params} />}
                  />
                </Stack>
              </LocalizationProvider>

              <Typography sx={{ display: 'flex' }} component="div" variant='div' >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Box sx={{ m: 1, position: 'relative' }}>
                    <Button variant="contained" sx={buttonSx("valueGamesByDate")} disabled={loading.valueGamesByDate} endIcon={<SendIcon />}
                      onClick={(e) => ClientFlow("valueGamesByDate", 
                      
                      `/GamesByDateClient/${currentDateLeft.getFullYear()}-${getMonth(currentDateLeft.getMonth())}-${getDay(currentDateLeft.getDate())}/${currentDateRight.getFullYear()}-${getMonth(currentDateRight.getMonth())}-${getDay(currentDateRight.getDate())}`,
                        e)}>
                      {success.valueGamesByDate ? <CheckIcon /> : <SaveIcon />}
                      Download Data
                    </Button>
                    {loading.valueGamesByDate && (
                      <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                      />
                    )}
                  </Box>
                </Box>
                <Switch
                  edge="end" onChange={handleToggle('bluetooth', "valueGamesByDate")} checked={checked.valueGamesByDate.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
                />
              </Typography>
            </ListItem>
            <Divider />

            {//Service::::::::  valueInjuredPlayersClient  
            }
            <ListItem className={styles.listItem}>
              <Typography className={styles.listItemhead} component="div" variant='div'>
                <ListItemIcon><WifiIcon /></ListItemIcon>
                <ListItemText id="switch-list-label-wifi" primary="InjuredPlayersClient"

                  secondary={<Typography component="div" variant='h5' className={styles.subtitle}
                  >https://api.sportsdata.io/v3/cbb/scores/json/InjuredPlayersClient</Typography>}
                />
              </Typography>
              <Typography sx={{ display: 'flex' }} component="div" variant='div' >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Box sx={{ m: 1, position: 'relative' }}>
                    <Button variant="contained" sx={buttonSx("valueInjuredPlayers")} disabled={loading.valueInjuredPlayers} endIcon={<SendIcon />}
                      onClick={(e) => ClientFlow("valueInjuredPlayers", "/InjuredPlayersClient",
                        e)}>
                      {success.valueInjuredPlayers ? <CheckIcon /> : <SaveIcon />}
                      Download Data
                    </Button>
                    {loading.valueInjuredPlayers && (
                      <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                      />
                    )}
                  </Box>
                </Box>
                <Switch
                  edge="end" onChange={handleToggle('bluetooth', "valueInjuredPlayers")} checked={checked.valueInjuredPlayers.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
                />
              </Typography>
            </ListItem>
            <Divider />

            {//Service::::::::  TeamGameStatsBySeasonClient  
            }
            <ListItem className={styles.listItem}>
              <Typography className={styles.listItemhead} component="div" variant='div'>
                <ListItemIcon><WifiIcon /></ListItemIcon>
                <ListItemText id="switch-list-label-wifi" primary="TeamGameStatsBySeasonClient"
                  secondary={<Typography component="div" variant='h5' className={styles.subtitle}
                  >https://api.sportsdata.io/v3/cbb/scores/json/teamGameStatsBySeasonClient</Typography>}
                />
              </Typography>
              <Typography sx={{ display: 'flex' }} component="div" variant='div' >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Box sx={{ m: 1, position: 'relative' }}>
                    <Button variant="contained" sx={buttonSx("valueTeamGameStatsBySeason")} disabled={loading.valueTeamGameStatsBySeason} endIcon={<SendIcon />}
                      onClick={(e) => Client("valueTeamGameStatsBySeason", "https://sportsdataclient.herokuapp.com/TeamGameStatsBySeasonClient/2022/268",
                        e)} >
                      {success.valueTeamGameStatsBySeason ? <CheckIcon /> : <SaveIcon />}
                      Download Data
                    </Button>
                    {loading.valueTeamGameStatsBySeason && (
                      <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                      />
                    )}
                  </Box>
                </Box>
                <Switch
                  edge="end" onChange={handleToggle('bluetooth', "valueTeamGameStatsBySeason")} checked={checked.valueTeamGameStatsBySeason.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
                />
              </Typography>
            </ListItem>
            <Divider />

            {//Service::::::::  LiveGameOddsLineMovementClient/  
            }
            <ListItem className={styles.listItem}>
              <Typography className={styles.listItemhead} component="div" variant='div'>
                <ListItemIcon><WifiIcon /></ListItemIcon>
                <ListItemText id="switch-list-label-wifi" primary="LiveGameOddsLineMovementClient"
                  secondary={<Typography component="div" variant='h5' className={styles.subtitle}
                  >https://api.sportsdata.io/v3/cbb/scores/json/LiveGameOddsLineMovementClient</Typography>}
                />
              </Typography>
              <Typography sx={{ display: 'flex' }} component="div" variant='div' >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Box sx={{ m: 1, position: 'relative' }}>
                    <Button variant="contained" sx={buttonSx("valueLiveGameOddsLineMovement")} disabled={loading.valueLiveGameOddsLineMovement} endIcon={<SendIcon />}
                      onClick={(e) => ClientFlow("valueLiveGameOddsLineMovement", "https://sportsdataclient.herokuapp.com/LiveGameOddsLineMovementClient/30460",
                        e)}>
                      {success.valueLiveGameOddsLineMovement ? <CheckIcon /> : <SaveIcon />}
                      Download Data
                    </Button>
                    {loading.valueLiveGameOddsLineMovement && (
                      <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                      />
                    )}
                  </Box>
                </Box>
                <Switch
                  edge="end" onChange={handleToggle('bluetooth', "valueLiveGameOddsLineMovement")} checked={checked.valueLiveGameOddsLineMovement.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
                />
              </Typography>
            </ListItem>
            <Divider />

            {//Service::::::::  BettingMetadataClient/  
            }
            <ListItem className={styles.listItem}>
              <Typography className={styles.listItemhead} component="div" variant='div'>
                <ListItemIcon><WifiIcon /></ListItemIcon>
                <ListItemText id="switch-list-label-wifi" primary="BettingMetadataClient"
                  secondary={<Typography component="div" variant='h5' className={styles.subtitle}
                  >https://api.sportsdata.io/v3/cbb/scores/json/BettingMetadataClient</Typography>}
                />
              </Typography>
              <Typography sx={{ display: 'flex' }} component="div" variant='div' >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Box sx={{ m: 1, position: 'relative' }}>
                    <Button variant="contained" sx={buttonSx("valueBettingMetadata")} disabled={loading.valueBettingMetadata} endIcon={<SendIcon />}
                      onClick={(e) => ClientFlow("valueBettingMetadata", "https://sportsdataclient.herokuapp.com/BettingMetadataClient",
                        e)}
                    >
                      {success.valueBettingMetadata ? <CheckIcon /> : <SaveIcon />}
                      Download Data
                    </Button>
                    {loading.valueBettingMetadata && (
                      <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                      />
                    )}
                  </Box>
                </Box>
                <Switch
                  edge="end" onChange={handleToggle('bluetooth', "valueBettingMetadata")} checked={checked.valueBettingMetadata.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
                />
              </Typography>
            </ListItem>
            <Divider />


            {//Service::::::::  BettingMarketsByGameIDClient/  
            }
            <ListItem className={styles.listItem}>
              <Typography className={styles.listItemhead} component="div" variant='div'>
                <ListItemIcon><WifiIcon /></ListItemIcon>

                <ListItemText id="switch-list-label-wifi" primary="BettingMarketsByGameIDClient"
                  secondary={<Typography component="div" variant='h5' className={styles.subtitle}
                  >https://api.sportsdata.io/v3/cbb/scores/json/BettingMarketsByGameIDClient</Typography>}
                />
              </Typography>
              <Typography sx={{ display: 'flex' }} component="div" variant='div' >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Box sx={{ m: 1, position: 'relative' }}>
                    <Button variant="contained" sx={buttonSx("valueBettingMarketsByGameID")} disabled={loading.valueBettingMarketsByGameID} endIcon={<SendIcon />}
                      onClick={(e) => ClientFlow("valueBettingMarketsByGameID", "https://sportsdataclient.herokuapp.com/BettingMarketsByGameIDClient/30479",
                        e)}
                    >
                      {success.valueBettingMarketsByGameID ? <CheckIcon /> : <SaveIcon />}
                      Download Data
                    </Button>
                    {loading.valueBettingMarketsByGameID && (
                      <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                      />
                    )}
                  </Box>
                </Box>
                <Switch
                  edge="end" onChange={handleToggle('bluetooth', "valueBettingMarketsByGameID")} checked={checked.valueBettingMarketsByGameID.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
                />
              </Typography>
            </ListItem>
            <Divider />
            {//Service::::::::  BettingMarketsClient/  
            }
            <ListItem className={styles.listItem}>
              <Typography className={styles.listItemhead} component="div" variant='div'>
                <ListItemIcon><WifiIcon /></ListItemIcon>
                <ListItemText id="switch-list-label-wifi" primary="BettingMarketsClient"
                  secondary={<Typography component="div" variant='h5' className={styles.subtitle}
                  >https://api.sportsdata.io/v3/cbb/scores/json/BettingMarketsClient</Typography>}
                />
              </Typography>
              <Typography sx={{ display: 'flex' }} component="div" variant='div' >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Box sx={{ m: 1, position: 'relative' }}>
                    <Button variant="contained" sx={buttonSx("valueBettingMarkets")} disabled={loading.valueBettingMarkets} endIcon={<SendIcon />}
                      onClick={(e) => ClientFlow("valueBettingMarkets", "https://sportsdataclient.herokuapp.com/BettingMarketsClient/4697",
                        e)}
                    >
                      {success.valueBettingMarkets ? <CheckIcon /> : <SaveIcon />}
                      Download Data
                    </Button>
                    {loading.valueBettingMarkets && (
                      <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                      />
                    )}
                  </Box>
                </Box>
                <Switch
                  edge="end" onChange={handleToggle('bluetooth', "valueBettingMarkets")} checked={checked.valueBettingMarkets.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
                />
              </Typography>
            </ListItem>
            <Divider />
            {//Service::::::::  StadiumsClient/  
            }
            <ListItem className={styles.listItem}>
              <Typography className={styles.listItemhead} component="div" variant='div'>
                <ListItemIcon><WifiIcon /></ListItemIcon>
                <ListItemText id="switch-list-label-wifi" primary="StadiumsClient"
                  secondary={<Typography component="div" variant='h5' className={styles.subtitle}
                  >https://api.sportsdata.io/v3/cbb/scores/json/StadiumsClient</Typography>}
                />
              </Typography>
              <Typography sx={{ display: 'flex' }} component="div" variant='div' >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Box sx={{ m: 1, position: 'relative' }}>
                    <Button variant="contained" sx={buttonSx("valueStadiums")} disabled={loading.valueStadiums} endIcon={<SendIcon />}
                      onClick={(e) => ClientFlow("valueStadiums", "/StadiumsClient",
                        e)}
                    >
                      {success.valueStadiums ? <CheckIcon /> : <SaveIcon />}
                      Download Data
                    </Button>
                    {loading.valueStadiums && (
                      <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                      />
                    )}
                  </Box>
                </Box>
                <Switch
                  edge="end" onChange={handleToggle('bluetooth', "valueStadiums")} checked={checked.valueStadiums.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
                />
              </Typography>
            </ListItem>
            <Divider />

          </List>


          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">
              {"Successfull"}
            </DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                The data has been downloaded successfully. You can review the Logs table.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} autoFocus>
                Agree
              </Button>
            </DialogActions>
          </Dialog>

        </section>

      </Layout>

    </div >


  )
}

export default Sync;

