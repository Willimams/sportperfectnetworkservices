import Head from 'next/head'
import Image from 'next/image'
import styles from '../../styles/Home.module.css'
import Link from 'next/link'
import Layout, { siteTitle } from '../../components/layout'
import utilStyles from '../../styles/utils.module.css'

//import * as React from 'react';
import React, { useReducer,useState } from 'react'

import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import Switch from '@mui/material/Switch';
import WifiIcon from '@mui/icons-material/Wifi';
import BluetoothIcon from '@mui/icons-material/Bluetooth';
import Divider from '@mui/material/Divider';
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';
import { green } from '@mui/material/colors';
import Button from '@mui/material/Button';
import Fab from '@mui/material/Fab';
import CheckIcon from '@mui/icons-material/Check';
import SaveIcon from '@mui/icons-material/Save';
import SendIcon from '@mui/icons-material/Send';


import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';


const Sync = ({ allPostsData }) => {

  const [open, setOpen] = React.useState(false);
  const [openOne, setOpenOne] = React.useState(false);

  const [checked, setChecked] = React.useState(['wifi']);
  const [checkedOne, setCheckedOne] = React.useState(['wifi']);

  const [loading, setLoading] = React.useState(false);
  const [loadingOne, setLoadingOne] = React.useState(false);

  const [loadings, setLoadings] = React.useState({
    valueCurrentSeason:false,
    valueTeamClient:false,
    valueTeamSchedule:false,
  
  });

  const [successs, setSuccesss] = React.useState({
    valueCurrentSeason:false,
    valueTeamClient:false,
    valueTeamSchedule:false
  });




  const [success, setSuccess] = React.useState(false);

  const [successOne, setSuccessOne] = React.useState(false);

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setChecked(newChecked);
  };


  /**Israel */
  const handleToggleOne = (value) => () => {
    const currentIndex = checkedOne.indexOf(value);
    const newChecked = [...checkedOne];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setCheckedOne(newChecked);
  };

  /**israel */
  const buttonSxtest = {
    ...(successs.valueCurrentSeason && {
      bgcolor: green[500],
      '&:hover': {
        bgcolor: green[700],
      },
    }),
  };
  const buttonSx = {
    ...(success && {
      bgcolor: green[500],
      '&:hover': {
        bgcolor: green[700],
      },
    }),
  };

 

  const buttonSxPrueba = (name) => {
    console.log(name);
    return  {...(successs[name] && {
      bgcolor: green[500],
      '&:hover': {
        bgcolor: green[700],
      },
    })};
  }

  /*Israel*/
  const buttonSxOne = {
    ...(successOne && {
      bgcolor: green[500],
      '&:hover': {
        bgcolor: green[700],
      },
    }),
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };


  const handleClickOpenOne = () => {
    setOpenOne(true);
  };

  const handleCloseOne = () => {
    setOpenOne(false);
  };

  const pulsar  = async (name_value,url,e) => {
    e.preventDefault();
    console.log(name_value);
    console.log(url);
    console.log("valores... : ", loadings[name_value]) 

    if (!loadings[name_value]) {

      setSuccesss({...successs,[name_value]:false});
      setLoadings({...loadings,[name_value]:true});
     // setSuccess(false);
     // setLoading(true);
      const photo = await fetch(
       url

      ).then((response) => response.json());
      if (photo.Season === 2022 && photo.StartYear === 2021) {
        handleClickOpen();
      } else {
      }

      setSuccesss({...successs,[name_value]:true});
      setLoadings({...loadings,[name_value]:false});
     // setSuccess(true);
     // setLoading(false);

    }

  }
  // CurrentSeasonClient
  const currentSeasonClient = async event => {
    event.preventDefault()

    if (!loadings.valueCurrentSeason) {

      setSuccesss({...successs,valueCurrentSeason:false});
      setLoadings({...loadings,valueCurrentSeason:true});
     // setSuccess(false);
     // setLoading(true);
      const photo = await fetch(
        `https://sportsdataclient.herokuapp.com/CurrentSeasonClient`

      ).then((response) => response.json());
      if (photo.Season === 2022 && photo.StartYear === 2021) {
        handleClickOpen();
      } else {
      }

      setSuccesss({...successs,valueCurrentSeason:true});
      setLoadings({...loadings,valueCurrentSeason:false});
     // setSuccess(true);
     // setLoading(false);

    }

  }

  const teamClient = async event => {
    event.preventDefault()

    console.log('loading : ', loading);
    if (!loading) {
      setSuccess(false);
      setLoading(true);
      const dataApi = await fetch(
        `https://sportsdataclient.herokuapp.com/TeamClient`

      ).then((response) => response.json());
      if (dataApi.continueFlow === "TRUE") {
        handleClickOpen();
      } else {
      }
      setSuccess(true);
      setLoading(false);

    }

  }

  // TeamClient /**Israel */
  const teamClientOne = async event => {
    event.preventDefault()

    if (!loadingOne) {
      setSuccessOne(false);
      setLoadingOne(true);
      const dataApi = await fetch(
        `https://sportsdataclient.herokuapp.com/TeamClient`

      ).then((response) => response.json());
      if (dataApi.continueFlow === "TRUE") {
        handleClickOpenOne();
      } else {
      }
      setSuccessOne(true);
      setLoadingOne(false);

    }

  }

  // TeamScheduleClient
  const teamScheduleClient = async event => {
    event.preventDefault()

    if (!loading) {
      setSuccess(false);
      setLoading(true);
      const dataApi = await fetch(
        `https://sportsdataclient.herokuapp.com/TeamScheduleClient`

      ).then((response) => response.json());
      if (dataApi.continueFlow === "TRUE") {
        handleClickOpen();
      } else {
      }
      setSuccess(true);
      setLoading(false);

    }

  }

  // BettingSplitsByGameIdClient
  const bettingSplitsByGameIdClient = async event => {
    event.preventDefault()

    if (!loading) {
      setSuccess(false);
      setLoading(true);
      const dataApi = await fetch(
        `https://sportsdataclient.herokuapp.com/BettingSplitsByGameIdClient/30478`

      ).then((response) => response.json());
      if (dataApi.continueFlow === "TRUE") {
        console.log(dataApi);
        handleClickOpen();
      } else {
        console.log("Error to download.");
      }
      setSuccess(true);
      setLoading(false);

    }

  }



  // TeamTrendClient
  const teamTrendClient = async event => {
    event.preventDefault()


    if (!loading) {
      setSuccess(false);
      setLoading(true);
      const dataApi = await fetch(
        `https://sportsdataclient.herokuapp.com/TeamTrendClient`

      ).then((response) => response.json());
      if (dataApi.continueFlow === "TRUE") {
        handleClickOpen();
      } else {
      }
      setSuccess(true);
      setLoading(false);

    }

  }

  // LeagueHierarchyClient
  const leagueHierarchyClient = async event => {
    event.preventDefault()

    if (!loading) {
      setSuccess(false);
      setLoading(true);
      const dataApi = await fetch(
        `https://sportsdataclient.herokuapp.com/LeagueHierarchyClient`

      ).then((response) => response.json());
      if (dataApi.continueFlow === "TRUE") {

        handleClickOpen();
      } else {
        console.log("Error to download.");
      }
      setSuccess(true);
      setLoading(false);

    }

  }


  // GameOddsLineMovementClient/
  const gameOddsLineMovementClient = async event => {
    event.preventDefault()

    if (!loading) {
      setSuccess(false);
      setLoading(true);
      const dataApi = await fetch(
        `https://sportsdataclient.herokuapp.com/GameOddsLineMovementClient/30478`

      ).then((response) => response.json());
      if (dataApi.continueFlow === "TRUE") {

        handleClickOpen();
      } else {
        console.log("Error to download.");
      }
      setSuccess(true);
      setLoading(false);

    }

  }


  // GamesByDateClient
  const gamesByDateClient = async event => {
    event.preventDefault()

    if (!loading) {
      setSuccess(false);
      setLoading(true);
      const dataApi = await fetch(
        `https://sportsdataclient.herokuapp.com/GamesByDateClient/2021-03-12`

      ).then((response) => response.json());
      if (dataApi.continueFlow === "TRUE") {

        handleClickOpen();
      } else {
        console.log("Error to download.");
      }
      setSuccess(true);
      setLoading(false);

    }

  }


  // InjuredPlayersClient
  const injuredPlayersClient = async event => {
    event.preventDefault()

    if (!loading) {
      setSuccess(false);
      setLoading(true);
      const dataApi = await fetch(
        `https://sportsdataclient.herokuapp.com/InjuredPlayersClient`

      ).then((response) => response.json());
      if (dataApi.continueFlow === "TRUE") {

        handleClickOpen();
      } else {
        console.log("Error to download.");
      }
      setSuccess(true);
      setLoading(false);

    }

  }


  // TeamGameStatsBySeasonClient/
  const teamGameStatsBySeasonClient = async event => {
    event.preventDefault()

    if (!loading) {
      setSuccess(false);
      setLoading(true);
      const dataApi = await fetch(
        `https://sportsdataclient.herokuapp.com/TeamGameStatsBySeasonClient/2022/268`

      ).then((response) => response.json());
      if (dataApi.continueFlow === "TRUE") {

        handleClickOpen();
      } else {
        console.log("Error to download.");
      }
      setSuccess(true);
      setLoading(false);

    }

  }


  // LiveGameOddsLineMovementServer/30458/
  const liveGameOddsLineMovementClient = async event => {
    event.preventDefault()

    if (!loading) {
      setSuccess(false);
      setLoading(true);
      const dataApi = await fetch(
        `https://sportsdataclient.herokuapp.com/LiveGameOddsLineMovementClient/30460`

      ).then((response) => response.json());
      if (dataApi.continueFlow === "TRUE") {

        handleClickOpen();
      } else {
        console.log("Error to download.");
      }
      setSuccess(true);
      setLoading(false);

    }

  }


  // //BettingMetadataClient
  const bettingMetadataClient = async event => {
    event.preventDefault()

    if (!loading) {
      setSuccess(false);
      setLoading(true);
      const dataApi = await fetch(
        `https://sportsdataclient.herokuapp.com/BettingMetadataClient`

      ).then((response) => response.json());
      if (dataApi.continueFlow === "TRUE") {

        handleClickOpen();
      } else {
        console.log("Error to download.");
      }
      setSuccess(true);
      setLoading(false);

    }

  }


  // //BettingMarketsByGameIDClient/
  const bettingMarketsByGameIDClient = async event => {
    event.preventDefault()

    if (!loading) {
      setSuccess(false);
      setLoading(true);
      const dataApi = await fetch(
        `https://sportsdataclient.herokuapp.com/BettingMarketsByGameIDClient/30479`

      ).then((response) => response.json());
      if (dataApi.continueFlow === "TRUE") {

        handleClickOpen();
      } else {
        console.log("Error to download.");
      }
      setSuccess(true);
      setLoading(false);

    }

  }


  // BettingMarketsClient
  const bettingMarketsClient = async event => {
    event.preventDefault()

    if (!loading) {
      setSuccess(false);
      setLoading(true);
      const dataApi = await fetch(
        `https://sportsdataclient.herokuapp.com/BettingMarketsClient/4697`

      ).then((response) => response.json());
      if (dataApi.continueFlow === "TRUE") {

        handleClickOpen();
      } else {
        console.log("Error to download.");
      }
      setSuccess(true);
      setLoading(false);

    }

  }


  // StadiumsClient
  const stadiumsClient = async event => {
    event.preventDefault()
    

    if (!loading) {
      setSuccess(false);
      setLoading(true);
      const dataApi = await fetch(
        `https://sportsdataclient.herokuapp.com/StadiumsClient`

      ).then((response) => response.json());
      if (dataApi.continueFlow === "TRUE") {

        handleClickOpen();
      } else {
        console.log("Error to download.");
      }
      setSuccess(true);
      setLoading(false);

    }

  }


  return (
    <Layout home>
      <Head>
        <title>Sport Perfect Network - Services</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <p>Available Services</p>

        <List
          sx={{ width: '100%', maxWidth: '80%', bgcolor: 'background.paper' }}
          subheader={<ListSubheader>Settings</ListSubheader>}
        >
          {//Service::::::::  CurrentSeason  
          }
          <ListItem>
            <ListItemIcon><WifiIcon /></ListItemIcon>
            <ListItemText id="switch-list-label-wifi" primary="Current Season" secondary="https://api.sportsdata.io/v3/cbb/scores/json/CurrentSeasonClient" />
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box sx={{ m: 1, position: 'relative' }}>
                <Button variant="contained" sx={buttonSxtest} disabled={loadings.valueCurrentSeason} endIcon={<SendIcon />} onClick={currentSeasonClient}>
                  {successs.valueCurrentSeason ? <CheckIcon /> : <SaveIcon />}
                  Download Data
                </Button>
                {loadings.valueCurrentSeason && (
                  <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                  />
                )}
              </Box>
            </Box>
            <Switch
              edge="end" onChange={handleToggle('bluetooth')} checked={checked.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
            />
          </ListItem>
          <Divider />

          {//Service::::::::  TeamClient  
          }
          <ListItem>
            <ListItemIcon><WifiIcon /></ListItemIcon>
            <ListItemText id="switch-list-label-wifi" primary="TeamClient" secondary="https://api.sportsdata.io/v3/cbb/scores/json/TeamClient" />
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box sx={{ m: 1, position: 'relative' }}>
                <Button variant="contained" sx={buttonSxPrueba("valueTeamClient")} disabled={loadings.valueTeamClient} endIcon={<SendIcon />} 
                onClick={(e) => pulsar("valueTeamClient","https://sportsdataclient.herokuapp.com/CurrentSeasonClient",
                e)}>
                  {successs.valueTeamClient ? <CheckIcon /> : <SaveIcon />}
                  Download Data
                </Button>
                {loadings.valueTeamClient && (
                  <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                  />
                )}
              </Box>
            </Box>
            <Switch
              edge="end" onChange={handleToggleOne('bluetooth')} checked={checkedOne.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
            />
          </ListItem>
          <Divider />


          {//Service::::::::  teamScheduleClient  
          }
          <ListItem>
            <ListItemIcon><WifiIcon /></ListItemIcon>
            <ListItemText id="switch-list-label-wifi" primary="TeamScheduleClient" secondary="** https://api.sportsdata.io/v3/cbb/scores/json/TeamScheduleClient" />
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box sx={{ m: 1, position: 'relative' }}>
                <Button variant="contained" sx={buttonSxPrueba("valueTeamSchedule")} disabled={loadings.valueTeamSchedule} endIcon={<SendIcon />}
                  onClick={(e) => pulsar("valueTeamSchedule","https://sportsdataclient.herokuapp.com/CurrentSeasonClient",
                   e)}>
                  {successs.valueTeamSchedule ? <CheckIcon /> : <SaveIcon />}
                  Download Data
                </Button>
                {loadings.valueTeamSchedule && (
                  <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                  />
                )}
              </Box>
            </Box>
            <Switch
              edge="end" onChange={handleToggle('bluetooth')} checked={checked.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
            />
          </ListItem>
          <Divider />

          {//Service::::::::  BettingSplitsByGameIdClient  
          }
          <ListItem>
            <ListItemIcon><WifiIcon /></ListItemIcon>
            <ListItemText id="switch-list-label-wifi" primary="BettingSplitsByGameIdClient" secondary=" https://api.sportsdata.io/v3/cbb/scores/json/BettingSplitsByGameIdClient" />
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box sx={{ m: 1, position: 'relative' }}>
                <Button variant="contained" sx={buttonSx} disabled={loading} endIcon={<SendIcon />} onClick={bettingSplitsByGameIdClient}>
                  {success ? <CheckIcon /> : <SaveIcon />}
                  Download Data
                </Button>
                {loading && (
                  <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                  />
                )}
              </Box>
            </Box>
            <Switch
              edge="end" onChange={handleToggle('bluetooth')} checked={checked.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
            />
          </ListItem>
          <Divider />


          {//Service::::::::  TeamTrendClient  
          }
          <ListItem>
            <ListItemIcon><WifiIcon /></ListItemIcon>
            <ListItemText id="switch-list-label-wifi" primary="TeamTrendClient" secondary="** https://api.sportsdata.io/v3/cbb/scores/json/TeamTrendClient" />
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box sx={{ m: 1, position: 'relative' }}>
                <Button variant="contained" sx={buttonSx} disabled={loading} endIcon={<SendIcon />} onClick={teamTrendClient}>
                  {success ? <CheckIcon /> : <SaveIcon />}
                  Download Data
                </Button>
                {loading && (
                  <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                  />
                )}
              </Box>
            </Box>
            <Switch
              edge="end" onChange={handleToggle('bluetooth')} checked={checked.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
            />
          </ListItem>
          <Divider />


          {//Service::::::::  LeagueHierarchyClient  
          }
          <ListItem>
            <ListItemIcon><WifiIcon /></ListItemIcon>
            <ListItemText id="switch-list-label-wifi" primary="LeagueHierarchyClient" secondary=" https://api.sportsdata.io/v3/cbb/scores/json/LeagueHierarchyClient" />
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box sx={{ m: 1, position: 'relative' }}>
                <Button variant="contained" sx={buttonSx} disabled={loading} endIcon={<SendIcon />} onClick={leagueHierarchyClient}>
                  {success ? <CheckIcon /> : <SaveIcon />}
                  Download Data
                </Button>
                {loading && (
                  <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                  />
                )}
              </Box>
            </Box>
            <Switch
              edge="end" onChange={handleToggle('bluetooth')} checked={checked.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
            />
          </ListItem>
          <Divider />


          {//Service::::::::  GameOddsLineMovementClient  
          }
          <ListItem>
            <ListItemIcon><WifiIcon /></ListItemIcon>
            <ListItemText id="switch-list-label-wifi" primary="GameOddsLineMovementClient" secondary="https://api.sportsdata.io/v3/cbb/scores/json/GameOddsLineMovementClient" />
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box sx={{ m: 1, position: 'relative' }}>
                <Button variant="contained" sx={buttonSx} disabled={loading} endIcon={<SendIcon />} onClick={gameOddsLineMovementClient}>
                  {success ? <CheckIcon /> : <SaveIcon />}
                  Download Data
                </Button>
                {loading && (
                  <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                  />
                )}
              </Box>
            </Box>
            <Switch
              edge="end" onChange={handleToggle('bluetooth')} checked={checked.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
            />
          </ListItem>
          <Divider />


          {//Service::::::::  GamesByDateClient  
          }
          <ListItem>
            <ListItemIcon><WifiIcon /></ListItemIcon>
            <ListItemText id="switch-list-label-wifi" primary="GamesByDateClient" secondary="https://api.sportsdata.io/v3/cbb/scores/json/GamesByDateClient" />
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box sx={{ m: 1, position: 'relative' }}>
                <Button variant="contained" sx={buttonSx} disabled={loading} endIcon={<SendIcon />} onClick={gamesByDateClient}>
                  {success ? <CheckIcon /> : <SaveIcon />}
                  Download Data
                </Button>
                {loading && (
                  <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                  />
                )}
              </Box>
            </Box>
            <Switch
              edge="end" onChange={handleToggle('bluetooth')} checked={checked.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
            />
          </ListItem>
          <Divider />


          {//Service::::::::  InjuredPlayersClient  
          }
          <ListItem>
            <ListItemIcon><WifiIcon /></ListItemIcon>
            <ListItemText id="switch-list-label-wifi" primary="InjuredPlayersClient" secondary="https://api.sportsdata.io/v3/cbb/scores/json/InjuredPlayersClient" />
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box sx={{ m: 1, position: 'relative' }}>
                <Button variant="contained" sx={buttonSx} disabled={loading} endIcon={<SendIcon />} onClick={injuredPlayersClient}>
                  {success ? <CheckIcon /> : <SaveIcon />}
                  Download Data
                </Button>
                {loading && (
                  <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                  />
                )}
              </Box>
            </Box>
            <Switch
              edge="end" onChange={handleToggle('bluetooth')} checked={checked.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
            />
          </ListItem>
          <Divider />


          {//Service::::::::  TeamGameStatsBySeasonClient  
          }
          <ListItem>
            <ListItemIcon><WifiIcon /></ListItemIcon>
            <ListItemText id="switch-list-label-wifi" primary="TeamGameStatsBySeasonClient" secondary="https://api.sportsdata.io/v3/cbb/scores/json/teamGameStatsBySeasonClient" />
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box sx={{ m: 1, position: 'relative' }}>
                <Button variant="contained" sx={buttonSx} disabled={loading} endIcon={<SendIcon />} onClick={teamGameStatsBySeasonClient}>
                  {success ? <CheckIcon /> : <SaveIcon />}
                  Download Data
                </Button>
                {loading && (
                  <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                  />
                )}
              </Box>
            </Box>
            <Switch
              edge="end" onChange={handleToggle('bluetooth')} checked={checked.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
            />
          </ListItem>
          <Divider />


          {//Service::::::::  LiveGameOddsLineMovementClient/  
          }
          <ListItem>
            <ListItemIcon><WifiIcon /></ListItemIcon>
            <ListItemText id="switch-list-label-wifi" primary="LiveGameOddsLineMovementClient" secondary="https://api.sportsdata.io/v3/cbb/scores/json/LiveGameOddsLineMovementClient" />
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box sx={{ m: 1, position: 'relative' }}>
                <Button variant="contained" sx={buttonSx} disabled={loading} endIcon={<SendIcon />} onClick={liveGameOddsLineMovementClient}>
                  {success ? <CheckIcon /> : <SaveIcon />}
                  Download Data
                </Button>
                {loading && (
                  <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                  />
                )}
              </Box>
            </Box>
            <Switch
              edge="end" onChange={handleToggle('bluetooth')} checked={checked.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
            />
          </ListItem>
          <Divider />


          {//Service::::::::  BettingMetadataClient/  
          }
          <ListItem>
            <ListItemIcon><WifiIcon /></ListItemIcon>
            <ListItemText id="switch-list-label-wifi" primary="BettingMetadataClient" secondary="https://api.sportsdata.io/v3/cbb/scores/json/BettingMetadataClient" />
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box sx={{ m: 1, position: 'relative' }}>
                <Button variant="contained" sx={buttonSx} disabled={loading} endIcon={<SendIcon />} onClick={bettingMetadataClient}>
                  {success ? <CheckIcon /> : <SaveIcon />}
                  Download Data
                </Button>
                {loading && (
                  <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                  />
                )}
              </Box>
            </Box>
            <Switch
              edge="end" onChange={handleToggle('bluetooth')} checked={checked.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
            />
          </ListItem>
          <Divider />


          {//Service::::::::  BettingMarketsByGameIDClient/  
          }
          <ListItem>
            <ListItemIcon><WifiIcon /></ListItemIcon>
            <ListItemText id="switch-list-label-wifi" primary="BettingMarketsByGameIDClient" secondary="https://api.sportsdata.io/v3/cbb/scores/json/BettingMarketsByGameIDClient" />
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box sx={{ m: 1, position: 'relative' }}>
                <Button variant="contained" sx={buttonSx} disabled={loading} endIcon={<SendIcon />} onClick={bettingMarketsByGameIDClient}>
                  {success ? <CheckIcon /> : <SaveIcon />}
                  Download Data
                </Button>
                {loading && (
                  <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                  />
                )}
              </Box>
            </Box>
            <Switch
              edge="end" onChange={handleToggle('bluetooth')} checked={checked.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
            />
          </ListItem>
          <Divider />


          {//Service::::::::  BettingMarketsClient/  
          }
          <ListItem>
            <ListItemIcon><WifiIcon /></ListItemIcon>
            <ListItemText id="switch-list-label-wifi" primary="BettingMarketsClient" secondary="https://api.sportsdata.io/v3/cbb/scores/json/BettingMarketsClient" />
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box sx={{ m: 1, position: 'relative' }}>
                <Button variant="contained" sx={buttonSx} disabled={loading} endIcon={<SendIcon />} onClick={bettingMarketsClient}>
                  {success ? <CheckIcon /> : <SaveIcon />}
                  Download Data
                </Button>
                {loading && (
                  <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                  />
                )}
              </Box>
            </Box>
            <Switch
              edge="end" onChange={handleToggle('bluetooth')} checked={checked.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
            />
          </ListItem>
          <Divider />

          {//Service::::::::  StadiumsClient/  
          }
          <ListItem>
            <ListItemIcon><WifiIcon /></ListItemIcon>
            <ListItemText id="switch-list-label-wifi" primary="StadiumsClient" secondary="https://api.sportsdata.io/v3/cbb/scores/json/StadiumsClient" />
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box sx={{ m: 1, position: 'relative' }}>
                <Button variant="contained" sx={buttonSx} disabled={loading} endIcon={<SendIcon />} onClick={stadiumsClient}>
                  {success ? <CheckIcon /> : <SaveIcon />}
                  Download Data
                </Button>
                {loading && (
                  <CircularProgress size={24} sx={{ color: green[500], position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px', }}
                  />
                )}
              </Box>
            </Box>
            <Switch
              edge="end" onChange={handleToggle('bluetooth')} checked={checked.indexOf('bluetooth') !== -1} inputProps={{ 'aria-labelledby': 'switch-list-label-bluetooth', }}
            />
          </ListItem>
          <Divider />

        </List>


        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"Successfull"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              The data has been downloaded successfully. You can review the Logs table.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} autoFocus>
              Agree
            </Button>
          </DialogActions>
        </Dialog>

      </section>

    </Layout>



  )
}

export default Sync;

