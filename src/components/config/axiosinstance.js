
import axios from "axios";
 
const AxiosInstance = axios.create({ 
    baseURL: `https://ec2-52-86-173-111.compute-1.amazonaws.com/`,
	//baseURL: `http://jsonplaceholder.typicode.com`, // Dirección de servicio en segundo plano
   // baseURL:`https://sportsdataclient.herokuapp.com`,
	timeout: 30000,
    //timeout: 10000, // El tiempo de espera de la solicitud es de 1 minuto
	responseType: "json",
	 withCredentials: false, // si se permiten cookies
	 /* Las siguientes dos propiedades se utilizan para
      establecer el número de nuevas solicitudes automáticas 
      y el tiempo de intervalo para el error o el tiempo de espera de la solicitud*/
	 reintentar: 2, // número de solicitudes
	 //retryInterval: 1000 // Intervalo de reintento
     retryInterval: 2000 // Intervalo de reintento
});
 // Interceptar antes de la solicitud
 AxiosInstance.interceptors.request.use(config => {
		return config
	},
	function(error) {
		 // Haz algo cuando algo sale mal
		return Promise.reject(error)
	}
);
 // Devuelve la interceptación de datos después de la solicitud
 AxiosInstance.interceptors.response.use(res => {
        var config = res.config;
        if(res.status==200){
            return res
        }else{
                         /*Si la configuración no existe o el atributo de
                          reintento no está establecido, se lanzará un error de promesa*/
            if (!config || !config.retry) return Promise.reject(res);
                         // Establecer una variable para registrar el número de nuevas solicitudes
            config.retryCount = config.retryCount || 0;
                         /* Verifica si el número de nuevas solicitudes 
                         supera el número de solicitudes que establecemos*/
            if (config.retryCount >= config.retry) {
                return Promise.reject(res);
            }
                         // El número de nuevas solicitudes aumenta automáticamente
            config.retryCount += 1;
                         // Crea una nueva Promesa para manejar la brecha de re-solicitud
            var back = new Promise(function(resolve) {
                                 console.log ("interfaz" + config.url + "error en la adquisición de datos, volver a solicitar")
                setTimeout(function() {
                    resolve();
                }, config.retryInterval|| 1);
            });
                         // Devuelve la entidad de axios, vuelve a intentar la solicitud
            return back.then(function() {
                return AxiosInstance(config);
            });
        }
    },
    function axiosRetryInterceptor(res) {
        var config = res.config;
                 // Si la configuración no existe o el atributo de reintento no está establecido, se lanzará un error de promesa
        if (!config || !config.retry) return Promise.reject(res);
                 // Establecer una variable para registrar el número de nuevas solicitudes
        config.retryCount = config.retryCount || 0;
                 // Verifica si el número de nuevas solicitudes supera el número de solicitudes que establecemos
        if (config.retryCount >= config.retry) {
            return Promise.reject(res);
        }
                 // El número de nuevas solicitudes aumenta automáticamente
        config.retryCount += 1;
                 // Crea una nueva Promesa para manejar la brecha de re-solicitud
        var back = new Promise(function(resolve) {
                         console.log ("interfaz" + config.url + "solicitud agotada, re-solicitud")
            setTimeout(function() {
                resolve();
            }, config.retryInterval|| 1);
        });
                 // Devuelve la entidad de axios, vuelve a intentar la solicitud
        return back.then(function() {
            return AxiosInstance(config);
        });
    }
);


export default AxiosInstance;